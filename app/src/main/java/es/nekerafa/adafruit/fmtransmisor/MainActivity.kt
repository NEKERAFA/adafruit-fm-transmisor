package es.nekerafa.adafruit.fmtransmisor

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi

private const val REQUEST_ENABLE_BT: Int = 20

class MainActivity : AppCompatActivity() {
    private var output: TextView? = null;
    private var bluetoothAdapter: BluetoothAdapter? = null;
    private val BluetoothAdapter.isDisabled: Boolean
        get() = !isEnabled

    private fun PackageManager.missingSystemFeature(name: String): Boolean = !hasSystemFeature(name);

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        output = findViewById(R.id.output) as TextView

        packageManager.takeIf { it.missingSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE) }?.also {
            Toast.makeText(this, "This device is not supported", Toast.LENGTH_SHORT).show()
            finish()
        }

        output!!.text = "> LE Device - Ok"

        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager.adapter

        bluetoothAdapter?.takeIf { it.isDisabled }?.apply {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        }

        bluetoothAdapter?.takeIf { it.isEnabled }?.apply {
            output!!.text = "${output!!.text}\n> Enabling bluetooth - Ok"
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Enable bluetooth to use this application", Toast.LENGTH_SHORT).show()
                finish()
            }

            output!!.text = "${output!!.text}\n> Enabling bluetooth - Ok"
        }
    }

    override fun onResume() {
        super.onResume()
        //output?.text = "Cambio de texto"
    }
}